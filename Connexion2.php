<?php
require("SessionStart.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>

  <title>Armada 2019 - Connexion ou Inscription</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link href="css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 450px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      padding-top: 20px;
      background-color: #f1f1f1;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
      position:absolute;
      bottom:0%;
      width:100%;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height:auto;} 
      }
  </style>
  </head>
  <body style="background-image: url('images/bateau 2.jpg'); background-repeat:no-repeat; background-size:cover;
      margin:0;">
  
  <nav class="navbar navbar-inverse">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>                        
        </button>
        <a class="navbar-brand" href="#">Logo</a>
      </div>
      <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav">
          <li class="active"><a href="index.php">Home</a></li>
          <li><a href="#">About</a></li>
          <li><a href="infobateau.php">Navires</a></li>
          <li><a href="#">Contact</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
          <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
        </ul>
      </div>
    </div>
  </nav>

  <div class="container-fluid text-center">  
      <form action="fonction2.php" method="post">
      <h2>Connexion</h2>
      <div>
          <label for="mail">e-mail :</label>
          <input type="email" id="email" name="user_email">

          <label for="mdp">Mot de passe :</label>
          <input type="password" id="mdp" name="user_mdp">

          <button type="submit" class="btn btn-default" name="connexion">Connexion</button>
      </div>
      </form>

      <br>

    <form action="fonction.php" method="post">
      <div>
          <h2>Inscription</h2>
          <label for="prenom">Prénom :</label>
          <input type="prenom" id="prenom" name="user_prenom">

          <label for="nom">Nom :</label>
          <input type="text" id="nom" name="user_nom">
      </div>

      <br>

      <div>
          <label for="email">e-mail :</label>
          <input type="email" id="email" name="user_email">

          <label for="mdp">Mot de passe :</label>
          <input type="password" id="mdp" name="user_mdp">
      <div>

      <div>
          <label for="start">Date de naissance</label>
          <input type="date" id="ddn" name="user_ddn"/>
      </div>

      <button type="submit" class="btn btn-default">Inscription</button>
    </form>

    <form action="fonction3.php" method="post">
      <button type="submit" class="btn btn-default" name="test2">test2</button>
    </form>

  </div>
  
  <script src="js/bootstrap.min.js> </script>
  
  </body>
  </html>