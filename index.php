<?php
require("SessionStart.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>

  <title>Armada 2019</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link href="css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 450px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      padding-top: 20px;
      background-color: #f1f1f1;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
      position:absolute;
      bottom:0px;
      width:100%;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height:auto;} 
    }
  </style>
</head>
<body style="background-image: url('images/bateau 2.jpg'); background-repeat:no-repeat; background-size:cover; margin:0;">

<?php

if(isset($_SESSION['user_email']) && $_SESSION['user_email'] != '4') {
  echo '<nav class="navbar navbar-inverse">';
  echo '<div class="container-fluid">';
      echo '<div class="navbar-header">';
        echo '<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">';
        echo '<span class="icon-bar"><span>';
        echo '<span class="icon-bar"></span>';
        echo '<span class="icon-bar"></span>';
        echo '</button>';
        echo '<a class="navbar-brand" href="#">Logo</a>'; 
      echo "</div>";
    echo '<div class="collapse navbar-collapse" id="myNavbar">';
      echo '<ul class="nav navbar-nav">';
        echo '<li class="active"><a href="#">Home</a></li>';
        echo '<li><a href="#">About</a></li>';
        echo '<li><a href="infobateau.php">Navires</a></li>';
        echo '<li><a href="#">Contact</a></li>';
      echo "</ul>";
      echo '<ul class="nav navbar-nav navbar-right">';
        echo '<li><a href="deco.php"><span class="glyphicon glyphicon-log-in"></span> Déconnexion</a></li>';
      echo '</ul>';
    echo "</div>";
    echo "</div>";
  echo "</nav>";
} else {
  echo '<nav class="navbar navbar-inverse">';
  echo '<div class="container-fluid">';
      echo '<div class="navbar-header">';
        echo '<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">';
        echo '<span class="icon-bar"><span>';
        echo '<span class="icon-bar"></span>';
        echo '<span class="icon-bar"></span>';
        echo '</button>';
        echo '<a class="navbar-brand" href="#">Logo</a>'; 
      echo "</div>";
    echo '<div class="collapse navbar-collapse" id="myNavbar">';
      echo '<ul class="nav navbar-nav">';
        echo '<li class="active"><a href="#">Home</a></li>';
        echo '<li><a href="#">About</a></li>';
        echo '<li><a href="infobateau.php">Navires</a></li>';
        echo '<li><a href="#">Contact</a></li>';
      echo "</ul>";
      echo '<ul class="nav navbar-nav navbar-right">';
        echo '<li><a href="Connexion2.php"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>';
      echo '</ul>';
    echo "</div>";
    echo "</div>";
  echo "</nav>";
}
?>

  
<div class="container-fluid text-center">    
  <div class="row content">
	
    <div class="text-center"> 
      <h1>Armada 2019</h1>
    </div>
    <div class="col-sm-6 text-left">
      <p><B><FONT SIZE=5><FONT COLOR="#000000"> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</FONT></B></FONT></p>  
    </div>
    
    <div class="col-sm-6 text-left">
      <p><B><FONT SIZE=5>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </B></FONT></p>
    </div>
    
    <div class="row content">
    <hr color="black">
    </div>
  
    <div class="push"></div>
	
  </div>

</div>



<form action="statut1.php" method="post">
  <button type="submit" class="btn btn-default" name="test">test</button>
</form>


<script src="js/bootstrap.min.js> </script>

</body>
</html>